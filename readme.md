# API BackEnd Test

API BackEnd construido con NodeJS y Express para poder disponibilizar servicios de datos "Indicadores Económicos" 

## Comenzando 🚀

_Estas son algunas de las instrucciones para poder montar localmente el servicio._


### Pre-requisitos 📋

_Lo necesario para poder desplegar localmente son:_

```
NPM v5.6.0 :: NODE v8.9.4
```
```
node app.js ó nodemon app.js
```

### Instalación 🔧

_Se tiene que ejecucar dentro de la carpeta el comando npm install, para que pueda instalar las dependencias_

```
npm install
```

### URIs y descripción 🔧

_Se definen las Uris para sus consultas_

_Valor especifico: Puede ser cualquiera "cobre|dolar|euro|ipc|ivp|oro|plata|uf|utm|yen". Ejemplo:_
```
http://localhost:3000/valores-especifico/dolar
```

_Valor especifico + Fecha: Puede ser cualquiera "cobre|dolar|euro|ipc|ivp|oro|plata|uf|utm|yen" fecha "DD-MM-AAAA". Ejemplo:_
```
http://localhost:3000/valores-fecha/dolar/09-01-2020
```

_Últimos Valores: Trae todos los valores y fechas. Ejemplo:_
```
http://localhost:3000/ultimos-valores/last
```

## Construido con 🛠️



* [Express](https://expressjs.com/) - El framework Node rest
* [NodeJS](https://nodejs.org/es/) - El framework de JavaScript

